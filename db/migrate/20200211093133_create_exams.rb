class CreateExams < ActiveRecord::Migration[6.0]
  def up
    create_table :exams do |t|
      t.string :title
      t.string :mapel
      t.string :duration
      t.float :nilai
      t.string :status
      t.string :label
      t.string :student_id

      t.timestamps
    end
    def down
      drop_table = exams 
    end
  end
end
