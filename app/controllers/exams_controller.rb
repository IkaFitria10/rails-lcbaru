class ExamsController < ApplicationController
  def new
    @exam= Exam.new
  end

  def create
    exam= Exam.new(resource_params)
    exam.save
    flash[:notice] = 'Selamat mengerjakan!'
    redirect_to exams_path
  end

  def edit
    @exam = Exam.find(params[:id])
  end

  def update
    id = params[:id]
    @exam = Exam.find(id)
    @exam.update(resource_params)
    redirect_to exams_path
  end

  def destroy
    id = params[:id]
    @exam = Exam.find(id)
    @exam.destroy
    flash[:notice] = 'Semangat terus!'
    redirect_to exams_path
  end

  def index
    @exams = Exam.all
  end

  def show
    id = params[:id]
    @exam = Exam.find(id)
  end

  def resource_params
    params.require(:exam).permit(:title, :mapel, :duration, :nilai, :status, :label, :student_id)
  end
end
